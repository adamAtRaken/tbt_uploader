#!/bin/bash

mkdir -p packaged_talks
rm packaged_talks/*.zip
let i=5
let len="$(find talks -name \*.pdf | wc -l)"
IFS=$'\n'
while true; do
  files="$(find talks  -name \*.pdf | tail -n $i | head -n 5)"

  if [ $i -gt $len ]; then
    break
  fi

  zip -j packaged_talks/talks_$i.zip $files

  let i=$i+5
done
